`(== 64 64)
(seed (in "/dev/urandom" (rd 8)))
(de randL (N)
   (make
      (do N
         (link (rand 0 255)) ) ) )
(de mono_blake2b (L O)
   (let Len (length L)
      (use R
         (native
            "libmonocypher.so"
            "crypto_blake2b_general"
            NIL
            (cons 'R (cons (cons O 'B O)))
            O
            0
            0
            (if L (cons NIL (cons Len) L) 0)
            Len )
         R ) ) )
(de wolf_blake2b_init (M O)
   (native
      "libwolfssl.so"
      "wc_InitBlake2b"
      NIL
      M
      O ) )
(de wolf_blake2b_update (M L)
   (let Len (length L)
      (native
         "libwolfssl.so"
         "wc_Blake2bUpdate"
         NIL
         M
         (if L (cons NIL (cons Len) L) 0)
         Len ) ) )
(de wolf_blake2b_final (M O)
   (use R
      (native
         "libwolfssl.so"
         "wc_Blake2bFinal"
         NIL
         M
         (cons 'R (cons (cons O 'B O)))
         O )
      R ) )
(de wolf_blake2b (L O)
   (let M (native "@" "malloc" 'N 380)
      (wolf_blake2b_init M O)
      (wolf_blake2b_update M L)
      (prog1
         (wolf_blake2b_final M O)
         (native "@" "free" NIL M) ) ) )

(for N (range 0 1024)
   (let D (randL N)
      (for O 64
         (test
            (mono_blake2b D O)
            (wolf_blake2b D O) ) ) ) )
(msg 'ok)
(bye)
