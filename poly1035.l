`(== 64 64)
(seed (in "/dev/urandom" (rd 8)))
(de rand32 NIL
   (make
      (do 32
         (link (rand 0 255)) ) ) )
(de randL (N)
   (make
      (do N
         (link (rand 0 255)) ) ) )
(de mono_poly (M K)
   (let (ML (length M)  R NIL)
      (native
         "libmonocypher.so"
         "crypto_poly1305"
         NIL
         '(R (16 B . 16))
         (ifn M 0 (cons NIL (cons ML) M))
         ML
         (cons NIL (32) K) )
      R ) )
(de w1olf_blake2b_init (M O)
   (native
      "libwolfssl.so"
      "wc_InitBlake2b"
      NIL
      M
      O ) )
(de wolf_poly_init (M K)
   (native
      "libwolfssl.so"
      "wc_Poly1305SetKey"
      NIL
      M
      (cons NIL (32) K)
      32 ) )
(de wolf_poly_update (M D)
   (let Len (length D)
      (native
         "libwolfssl.so"
         "wc_Poly1305Update"
         NIL
         M
         (if D (cons NIL (cons Len) D) 0)
         Len ) ) )
(de wolf_poly_final (M)
   (use R
      (native
         "libwolfssl.so"
         "wc_Poly1305Final"
         NIL
         M
         '(R (16 B . 16)) )
      R ) )
(de wolf_poly (D K)
   (let M (native "@" "malloc" 'N 512)
      (wolf_poly_init M K)
      (wolf_poly_update M D)
      (prog1
         (wolf_poly_final M)
         (native "@" "free" NIL M) ) ) )
# (println (mono_poly (range 1 100) (range 1 32)))
# (println (wolf_poly (range 1 100) (range 1 32)))
(for N (range 0 8192)
   (let (D (randL N)  K (rand32))
      (test
         (mono_poly D K)
         (wolf_poly D K) ) ) )



(msg 'ok)
(bye)
